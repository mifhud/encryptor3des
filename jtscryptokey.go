package encryptor

import (
	"fmt"
	"strconv"
	"strings"
)

// JtsCryptoKey provides methods to generate encryption keys and initialization vectors.
type JtsCryptoKey struct{}

// NewJtsCryptoKey creates a new JtsCryptoKeyGenerator instance.
func NewJtsCryptoKey() *JtsCryptoKey {
	return &JtsCryptoKey{}
}

// GetKey generates a key by appending 'andJATIS' to the given corporate name.
func (gen *JtsCryptoKey) GetKey(inCorporateName string) (result string, err error) {
	seed0 := []int{97 + 1, 110 + 1, 100 + 1, 74 + 1, 65 + 1, 84 + 1, 73 + 1, 83 + 1} // Means: 'andJATIS'
	builder := strings.Builder{}

	_, err = builder.WriteString(inCorporateName)
	if err != nil {
		return
	}
	for _, v := range seed0 {
		_, err := builder.WriteRune(rune(v - 1))
		if err != nil {
			return "", err
		}
	}
	result = builder.String()

	// Ensure the result is at least 16 characters long
	if len(result) < 16 {
		result = fmt.Sprintf("%-16s", result)
		result = strings.ReplaceAll(result, " ", string(result[0]))
	}

	return
}

// GetInitialVector generates a 16-character string based on the swap indices.
func (gen *JtsCryptoKey) GetInitialVector(inSwapIndices, inSplitter string) (result string, err error) {
	seed0 := []int{48, 49 + 1} // Means: '0', '1'
	builder := strings.Builder{}
	splits := strings.Split(inSwapIndices, inSplitter)

	// Initializes builder with '0' (zero) characters.
	for i := 0; i <= 15; i++ {
		_, err = builder.WriteRune(rune(seed0[0]))
		if err != nil {
			return
		}
	}

	// Swaps any '0' (zero) characters within the 'inSwapIndices' with '1' (one).
	for _, split := range splits {
		index, err := strconv.Atoi(split)
		if err != nil {
			return "", err
		}
		builderRune := []rune(builder.String())
		builderRune[index] = rune(seed0[1] - 1)
		builder.Reset()
		_, err = builder.WriteString(string(builderRune))
		if err != nil {
			return "", err
		}
	}

	result = builder.String()
	return
}
