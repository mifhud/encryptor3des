package encryptor

// ApplicationSetting holds encryption settings
type ApplicationSetting struct {
	EncryptKey    string
	EncryptVector string
}

// Encryptor struct will hold the application settings and methods
type Encryptor struct {
	settings ApplicationSetting
}

// NewTripleDesEncryptor is the constructor for Encryptor
func NewTripleDesEncryptor(settings ApplicationSetting) *Encryptor {
	return &Encryptor{settings: settings}
}

// TripleDESDecrypt decrypts the given data using 3DES encryption
func (e *Encryptor) Decrypt(inData string) (result string, err error) {
	var key, vector string

	jtsCryptoKey := NewJtsCryptoKey()
	key, err = jtsCryptoKey.GetKey(e.settings.EncryptKey)
	if err != nil {
		return
	}

	vector, err = jtsCryptoKey.GetInitialVector(e.settings.EncryptVector, ",")
	if err != nil {
		return
	}

	jtsCryptoNet := NewJtsCryptoNet(key, vector)

	result, err = jtsCryptoNet.TripleDESDecrypt(inData)
	if err != nil {
		return
	}

	result, err = jtsCryptoNet.HexToString(result)
	if err != nil {
		return
	}

	return
}

// TripleDESEncrypt encrypts the given data using 3DES encryption
func (e *Encryptor) Encrypt(inData string) (result string, err error) {
	var key, vector string

	jtsCryptoKey := NewJtsCryptoKey()

	key, err = jtsCryptoKey.GetKey(e.settings.EncryptKey)
	if err != nil {
		return
	}

	vector, err = jtsCryptoKey.GetInitialVector(e.settings.EncryptVector, ",")
	if err != nil {
		return
	}

	jtsCryptoNet := NewJtsCryptoNet(key, vector)

	result, err = jtsCryptoNet.TripleDESEncrypt(inData)
	if err != nil {
		return
	}

	return
}
