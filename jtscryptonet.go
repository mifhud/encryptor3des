package encryptor

import (
	"bytes"
	"crypto/cipher"
	"crypto/des"
	"encoding/hex"
)

// JtsCryptoNet provides 3DES encryption and decryption methods.
type JtsCryptoNet struct {
	key string
	iv  string
}

// NewJtsCryptoNet creates a new JtsCryptoKey instance.
func NewJtsCryptoNet(key, iv string) *JtsCryptoNet {
	return &JtsCryptoNet{key: key, iv: iv}
}

// HexToString converts a hexadecimal string to a regular string.
func (s *JtsCryptoNet) HexToString(hexStr string) (string, error) {
	bytes, err := hex.DecodeString(hexStr)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// hexToByte converts a hexadecimal string to a byte slice.
func (s *JtsCryptoNet) hexToByte(hexStr string) ([]byte, error) {
	return hex.DecodeString(hexStr)
}

// stringToHex converts a string to a hexadecimal string.
func (e *JtsCryptoNet) stringToHex(s string) string {
	return hex.EncodeToString([]byte(s))
}

// TripleDESEncrypt encrypts data using 3DES encryption in CBC mode.
func (s *JtsCryptoNet) TripleDESEncrypt(data string) (string, error) {
	dataHex := s.stringToHex(data)
	plaintext, err := s.hexToByte(dataHex)
	if err != nil {
		return "", err
	}

	keyHex := s.stringToHex(s.key)[:32] + s.stringToHex(s.key)[:16]
	tdesKey, err := s.hexToByte(keyHex)
	if err != nil {
		return "", err
	}

	myIV, err := s.hexToByte(s.iv)
	if err != nil {
		return "", err
	}

	block, err := des.NewTripleDESCipher(tdesKey)
	if err != nil {
		return "", err
	}

	if len(dataHex)%16 != 0 {
		plaintext = pkcs5Padding(plaintext, block.BlockSize())
	}

	ciphertext := make([]byte, len(plaintext))
	cbcEncrypter := cipher.NewCBCEncrypter(block, myIV)
	cbcEncrypter.CryptBlocks(ciphertext, plaintext)

	return bytesToHex(ciphertext), nil
}

// TripleDESDecrypt decrypts data using 3DES decryption in CBC mode.
func (s *JtsCryptoNet) TripleDESDecrypt(data string) (string, error) {
	plaintext, err := s.hexToByte(data)
	if err != nil {
		return "", err
	}

	keyHex := s.stringToHex(s.key)[:32] + s.stringToHex(s.key)[:16]
	tdesKey, err := s.hexToByte(keyHex)
	if err != nil {
		return "", err
	}

	myIV, err := s.hexToByte(s.iv)
	if err != nil {
		return "", err
	}

	block, err := des.NewTripleDESCipher(tdesKey)
	if err != nil {
		return "", err
	}

	cbcDecrypter := cipher.NewCBCDecrypter(block, myIV)
	ciphertext := make([]byte, len(plaintext))
	cbcDecrypter.CryptBlocks(ciphertext, plaintext)

	ciphertext = pkcs5Unpadding(ciphertext)

	decodedStr, err := s.HexToString(bytesToHex(ciphertext))
	if err != nil {
		return "", err
	}

	return s.stringToHex(decodedStr), nil
}

// bytesToHex converts a byte slice to a hexadecimal string.
func bytesToHex(bytes []byte) string {
	return hex.EncodeToString(bytes)
}

// Helper functions for padding and unpadding (PKCS5).
func pkcs5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func pkcs5Unpadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}
